#!/bin/bash
#Run to build all the custom Python classes for the interfaces from the .ui files
#Using PyQt5
echo "Building all the Python classes for the interfaces from the .ui and .qrc files"
shopt -s nullglob
for filepath in *.ui; do
    filename=$(basename $filepath .ui)
    echo "Building 'Ui_${filename}.py'"
    pyuic6 $filepath -o Ui_${filename}.py 
done
for filepath in *.qrc; do
    filename=$(basename $filepath .qrc)
    echo "Building '${filename}_rc.py'"
    pyrcc5 $filepath -o ${filename}_rc.py 
done
