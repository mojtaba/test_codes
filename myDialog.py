import sys
from PyQt5.QtWidgets import QApplication
from Ui_progress_dialog import Ui_ProcessMonitoringProgress
from PyQt5 import QtWidgets,QtCore
from PyQt5.QtGui import QPixmap

from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import QDialog
from PyQt5.QtCore import pyqtSignal

import time
import logging

import clim_chamber # We need to figure out proper way for importing that

HOST = "192.168.4.199"
PORT = 888

logger = logging.getLogger('clim_chamber')


class ProgressDialog(QDialog):
    """Dialog to show the progress of a process.
    """
    update_progress = pyqtSignal(str, float)
    increase_progress = pyqtSignal(str)

    def __init__(self):
        QDialog.__init__(self)
        # This creates the UI following the python class which was generated
        # based in the .ui file
        self.ui = Ui_ProcessMonitoringProgress()
        self.ui.setupUi(self)
    
        self.show()

        self.counter = 0 # time elapsed
        self.tmpr = 0    # measured temperature
        self.tmprsp = 0  # set point temperature
        self.hmdty = 0   # meaured humidity
        self.hmdtysp = 0 # set point humidity
        self.cooltime = 0 # time to reach the temperature
        self.dewtemp = 0  # ew time temperature
        self.time_estimate = 0         

        self.image()
        self.timeElapsed()
    
        self.ui.progressBar.setValue(0)

        self.update_progress.connect(self.updateProgress)
        self.increase_progress.connect(self.increaseProgress)
        self.connect_chamber()
        self.runProcess()  # Start the process that we want to "monitor"
  
       # self.close()
        self.closeWindow()
 
    def updateProgress(self, message, progress_percentage):
    
        self.ui.label.setText(message)
        self.ui.label.setFont(QFont('Arial', 16))
        self.ui.progressBar.setValue(progress_percentage)
        QApplication.processEvents()

    def increaseProgress(self, message):
   
        progress_percentage = self.ui.progressBar.value()  # Get current value and increase it
        self.update_progress.emit(message, progress_percentage)
        

    def updateTemperature(self):
        self.ltemp = self.findChild(QtWidgets.QLabel, 'label_1')
        self.ltemp.setFont(QFont('Arial', 16))
        self.ltemp.setStyleSheet("border: 1px solid black;")
        self.ltemp.setText("Temperature now: %.1f" % self.tmpr) #
        
    def updateTemperatureSetPoint(self):
        self.ltempsp = self.findChild(QtWidgets.QLabel, 'label_3')
        self.ltempsp.setFont(QFont('Arial', 16))
        self.ltempsp.setStyleSheet("border: 1px solid black;")
        self.ltempsp.setText("Temperature set point: %.1f" % self.tmprsp) #
        
        
    def updateHumidity(self):
        self.lhmdty = self.findChild(QtWidgets.QLabel, 'label_2')
        self.lhmdty.setFont(QFont('Arial', 16))
        self.lhmdty.setStyleSheet("border: 1px solid black;")
        self.lhmdty.setText("Humidity now: %.1f" % self.hmdty) #

    def updateHumiditySetPoint(self):
        self.lhmdtysp = self.findChild(QtWidgets.QLabel, 'label_4')
        self.lhmdtysp.setFont(QFont('Arial', 16))
        self.lhmdtysp.setStyleSheet("border: 1px solid black;")
        self.lhmdtysp.setText("Humidity set point: %.1f" % self.hmdtysp) #

    def timeElapsed(self):
        self.timer = QTimer()
        self.timer.setInterval(self.cooltime) # time elapsed until cooling time!
        self.timer.timeout.connect(self.recurring_timer)
        self.timer.start()

    def updateCoolTime(self):
        self.lctime = self.findChild(QtWidgets.QLabel, 'label_6')
        self.lctime.setFont(QFont('Arial', 16))
        self.lctime.setStyleSheet("border: 1px solid black;")
        self.lctime.setText("Time estimated: %d" % self.cooltime) #

    def updateDewPointTemperature(self):
        self.dewp = self.findChild(QtWidgets.QLabel, 'label_7')
        self.dewp.setFont(QFont('Arial', 16))
        self.dewp.setStyleSheet("border: 1px solid black;")
        self.dewp.setText("Dew point temp.: %.1f" % self.dewtemp) #

    def connect_chamber(self):
        """Brief:  Connect with the TCP server of climatic chamber."""
        self.clim_cham = clim_chamber.ClimaticChamberController(HOST, PORT)
        if self.clim_cham.connect():
            logger.info('Succesfully connected to the Climatic Chamber!')
        else:
            delattr(self, 'clim_cham')
        logger.debug('Leaving connect_chamber')

    def get_temp(self):

        if not hasattr(self, 'clim_cham'):
            logger.error("Chamber not connected")
        else:
            response = self.clim_cham.read_value("temperature_meas")
            self.tmpr = float(response)
            response_tmpsp = self.clim_cham.read_value("temperature")
            self.tmprsp = float(response_tmpsp)
            response_dewp = self.clim_cham.read_value("dewpoint")
            self.dewtemp = float(response_dewp)
        
            response_hmdty = self.clim_cham.read_value("humidity_sens")
            self.hmdty = float(response_hmdty)
            
            response_hmdtysp = self.clim_cham.read_value("humidity")
            self.hmdtysp = float(response_hmdtysp)
        
            logger.debug(f'Get returned: {response}')
            if not response:
                logger.error("Unsuccesful call")
            else:
                logger.info(f'"temperature_meas" value is: {response}')

    def go_cold(self):

        if not hasattr(self, 'clim_cham'):
            logger.error("Chamber not connected")
            return 0
        else:
            cooling_time = self.clim_cham.go_cold()
            self.cooltime = int(cooling_time)
            logger.debug(f'Get returned: {cooling_time}')
            if not cooling_time:
                logger.error("Unsuccesful call")
            else:
                logger.info(f'Cooling time will be: {cooling_time} s')
            return cooling_time
           # self.cooltime = float(cooling_time)
            
    
    def runProcess(self):
        """Runs a process and updates the progress bar periodically
        """
 
        logger.info(f'Going cold')
        logger.info(f'Cool time before {self.cooltime}')
        self.go_cold()
        logger.info(f'Cool time after {self.cooltime}')
    
      

        for index in range(self.cooltime):  # Process running

          #  self.time_estimate = self.cooltime      
            self.get_temp()
            self.updateTemperature()
            self.updateTemperatureSetPoint()
            self.updateCoolTime()
            self.updateDewPointTemperature()
            self.updateHumidity()
            self.updateHumiditySetPoint()
            self.update_progress.emit(f"", 100 * (index+1)/ self.cooltime)
            time.sleep(1)
           
      #  self.ui.progressBar.hide()

    def recurring_timer(self):
        if self.counter <= self.cooltime:
            self.counter += 1
            self.ltime = self.findChild(QtWidgets.QLabel, 'label_5')
            self.ltime.setStyleSheet("border: 1px solid black;")
            self.ltime.setFont(QFont('Arial', 16))
            self.ltime.setText("Elapsed time: %d" % self.counter)
 
    def closeWindow(self):
        if self.counter >= self.cooltime:
            self.close()

    def image(self):
        self.limage = self.findChild(QtWidgets.QLabel,'label_image')
        self.limage.setPixmap(QPixmap("climatic_chamber_image.jpg"))
        self.vbox = QVBoxLayout()
        self.vbox.addWidget(self.limage)

# For development, this dialog can be run directly by running this file.

if __name__ == "__main__":
    app = QApplication(sys.argv)

    app.progressDialog = ProgressDialog()
    #app.progressDialog.exec()
    print("End.")
